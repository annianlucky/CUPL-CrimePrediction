# -*- coding: utf-8 -*-
'''
将训练数据使用jieba分词工具进行分词。并且剔除stopList中的词。
得到词表：
        词表的每一行的内容为：词 词的序号 词的频次
'''


import json
import jieba
import re
from tqdm import tqdm

InputTrainFile = 'law_data/my_traindata.json'
InputTestFile = 'law_data/my_testdata.json'
InputALLFile = 'law_data/my_alldata.json'
InputStopwordFile = 'stopword.txt'
OutputWordLabelFile = 'law_data/Output/wordLabel.txt'
OutputLengthFile = 'law_data/Output/length.txt'





def read_stopword(file):
    data = open(file, 'r', encoding='utf_8').read().split('\n')

    return data



def main():
    worddict = {}
    stoplist = read_stopword(InputStopwordFile)
    datas = open(InputALLFile, 'r', encoding='utf_8').read().split('\n')
    datas = list(filter(None, datas))#把序列中的False值，如空字符串、False、[]、None、{}、()等等都丢弃。
    data_num = len(datas)
    len_dic = {}
    for line in datas:
        line = json.loads(line)#把json格式数据转换为字典
        title = str(list(line.values())[0])
        title_seg = jieba.cut(title, cut_all=False)
        # print(type(title_seg))
        length = 0
        for w in title_seg:
            w = re.sub(r"\s+", "", w)
            # w = w.replace(" ", "").replace("\n", "")
            if w not in stoplist:#剔除无用词
                for stopstr in stoplist:
                    stop_flag = 0
                    if stopstr in w:
                        stop_flag =1
                        break
                if stop_flag == 0:
                    # print(w)
                    length += 1
                    if w in worddict:  # 统计每个词的词频
                        worddict[w] += 1
                    else:
                        worddict[w] = 1



        if length in len_dic:
            len_dic[length] += 1
        else:
            len_dic[length] = 1

    wordlist = sorted(worddict.items(), key=lambda item:item[1], reverse=True)
    f = open(OutputWordLabelFile, 'w', encoding='utf_8')
    ind = 0
    for t in wordlist:
        d = t[0] + ' ' + str(ind) + ' ' + str(t[1]) + '\n'
        ind += 1
        f.write(d)

    for k, v in len_dic.items():
        len_dic[k] = round(v * 1.0 / data_num, 3)
    len_list = sorted(len_dic.items(), key=lambda item:item[0], reverse=True)
    f = open(OutputLengthFile, 'w')
    for t in len_list:
        d = str(t[0]) + ' ' + str(t[1]) + '\n'
        f.write(d)



if __name__ == "__main__":
    main()
    #下面需要去掉txt中BOM字符，否则会报json.decoder.JSONDecodeError错
    BOM = b'\xef\xbb\xbf'
    with open(OutputWordLabelFile, 'r+b') as f:
        if BOM == f.read(3):
            content = f.read()
            f.seek(0)
            f.write(content)
            f.truncate()
    #去掉BOM字符完成

    #下面需要去掉txt中BOM字符，否则会报json.decoder.JSONDecodeError错
    BOM = b'\xef\xbb\xbf'
    with open(OutputLengthFile, 'r+b') as f:
        if BOM == f.read(3):
            content = f.read()
            f.seek(0)
            f.write(content)
            f.truncate()
    #去掉BOM字符完成
