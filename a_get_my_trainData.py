# -*- coding: utf-8 -*-
'''
从原数据train.json和test.json中选取部分数据，输出到my_xxxdata.json中。
各类别数量统计在字典WantedClas中，且各个类别的数量为WantedNum，数值来源为实测。

统计结果：
【train.json】——共：1710856，其中单类别1659634，多类别51222。
【test.json】——共：217016，其中单类别197542，多类别19474。
综上，过滤掉多类别的数据。
'''
import jieba
import json
import random


Input_TrainJsonFile = 'law_data/Initial/train.json'
Input_TestJsonFile = 'law_data/Initial/test.json'
Output_MyTainJsonFile = 'law_data/my_traindata.json'
Output_MyTestJsonFile = 'law_data/my_testdata.json'
Output_StopWordFile = 'stopword.txt'

WantedClass = {'盗窃': 0, '故意伤害': 0, '诈骗': 0, '寻衅滋事': 0, '容留他人吸毒': 0,
               '危险驾驶':0, '信用卡诈骗':0, '抢劫':0, '走私、贩卖、运输、制造毒品':0, '交通肇事':0}
WantedNum_train = 20000
WantedNum_test = 2300


def main(InputFile,OutputFile,WantNum):
    Datas = open(InputFile , 'r', encoding='utf_8').readlines()
    # random.shuffle(Datas)#打乱顺序
    f = open(OutputFile , 'w', encoding='utf_8')
    category_202 = {}

    for line in Datas:
        data = json.loads(line)
        # print(data['meta']['accusation'],data['fact'])
        if len(data['meta']['accusation']) == 1:#只考虑单类别
            cla = data['meta']['accusation'][0]
            if cla in WantedClass and WantedClass[cla] < WantNum:
                category_10 = {}
                category_10[cla] = data['fact']
                json_data = json.dumps(category_10, ensure_ascii=False)
                f.write(json_data)
                f.write('\n')
                WantedClass[cla] += 1
    print(WantedClass)


def combinelist(list_1,list_2):
    return list_1 + list_2


if __name__ == '__main__':
    #分别生成训练集和测试集
    # main(Input_TrainJsonFile,Output_MyTainJsonFile,WantedNum_train)
    # main(Input_TestJsonFile,Output_MyTestJsonFile,WantedNum_test)

    #组合两个数据集，生成my_alldata
    Datas_mytrain = open(Output_MyTainJsonFile, 'r', encoding='utf_8').readlines()
    Datas_mytest = open(Output_MyTestJsonFile, 'r', encoding='utf_8').readlines()
    f = open('law_data/my_alldata.json', 'w', encoding='utf_8')
    for line in combinelist(Datas_mytrain,Datas_mytest):
        data = json.loads(line)
        json_data = json.dumps(data, ensure_ascii=False)
        f.write(json_data)
        f.write('\n')
