# -*- coding:utf-8 -*-

import torch
import os
import torch.nn as nn
import numpy as np
import time
from d_model import textCNN
import c_sen2inds
import jieba
import re
import sys
import io
import codecs
import logging

# sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
type = sys.stdout.encoding
sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
maxLen = 50
# 设置编码方式为UTF-8
# sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

word2ind, ind2word = c_sen2inds.get_worddict('law_data/Output/wordLabel.txt')
label_w2n, label_n2w = c_sen2inds.read_labelFile('label.txt')
jieba.setLogLevel(logging.INFO)

textCNN_param = {
    'vocab_size': len(word2ind),
    'embed_dim': 60,
    'class_num': len(label_w2n),
    "kernel_num": 16,
    "kernel_size": [3, 4, 5],
    "dropout": 0.5,
}


def get_valData(file):
    datas = open(file, 'r').read().split('\n')
    datas = list(filter(None, datas))

    return datas

def read_stopword(file):
    data = open(file, 'r', encoding='utf_8').read().split('\n')

    return data

def parse_net_result(out):
    score = max(out)
    label = np.where(out == score)[0][0]

    return label, score


def main(sentence):
    # init net
    print('init net...')
    net = textCNN(textCNN_param)
    weightFile = 'F:/lxg_TextCNN_classify/textCNN.pkl'
    if os.path.exists(weightFile):
        print('load weight')
        net.load_state_dict(torch.load(weightFile, map_location='cpu'))
    else:
        print('No weight file!')
        exit()
    print(net)

    # net.cuda()
    net.eval()

    numAll = 0
    numRight = 0
    testData = get_valData('law_data/Output/valdata_vec.txt')
    # sentence = "公诉机关指控：2016年3月28日20时许，被告人颜某在本市洪山区马湖新村足球场马路边捡拾到被害人谢某的VIVOX5手机一部，并在同年3月28日21时起，分多次通过支付宝小额免密支付功能，秘密盗走被害人谢某支付宝内人民币3723元。案发后，被告人颜某家属已赔偿被害人全部损失，并取得谅解。公诉机关认为被告人颜某具有退赃、取得谅解、自愿认罪等处罚情节，建议判处被告人颜某一年以下××、××或者××，并处罚金。\r\n"
    # sentence = "李小果说她会爱我一辈子"
    print('【待预测输入为】: ',sentence)
    title_seg = jieba.cut(sentence, cut_all=False)
    stoplist = read_stopword('stopword.txt')
    title_ind = []
    for w in title_seg:
        w = re.sub(r"\s+", "", w)
        # w = w.replace(" ", "").replace("\n", "")
        if w not in stoplist:  # 剔除无用词
            for stopstr in stoplist:
                stop_flag = 0
                if stopstr in w:
                    stop_flag = 1
                    break
            if stop_flag == 0:
                if w in word2ind.keys():
                    title_ind.append(word2ind[w])  # 第一个数字是类别，其余为切片后的句子向量。
                else:
                    title_ind.append('0')

    length = len(title_ind)
    if length > maxLen:
        title_ind = title_ind[0:(maxLen)]
    if length < maxLen:
        title_ind.extend([0] * (maxLen - length))
    # print(title_ind)
    if len(title_ind) != 50: print('Slice error!')
    sentence = np.array([int(x) for x in title_ind[0:50]])
    sentence = torch.from_numpy(sentence)
    predict = net(sentence.unsqueeze(0).type(torch.LongTensor)).cpu().detach().numpy()[0]
    label_pre, score = parse_net_result(predict)
    print('【预测类别为】: ',label_n2w[label_pre])



if __name__ == "__main__":
    print("预测支持类别：盗窃、故意伤害、诈骗、寻衅滋事、容留他人吸毒、危险驾驶、信用卡诈骗、抢劫、走私贩卖运输制造毒品、交通肇事")
    # print("")
    while(1):
        sentence = input("请输入待预测的裁判文书：")
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        # sentence = "李小果说她会爱我一辈子"
        main(sentence)
        print("***** 版权所有：《基于自然语言处理的法律判决预测研究》*****")
        print("*************** CUPL - 李小果 - 211224026 ***************")

