#-*- coding: utf_8 -*-
#这一步将中文标题转化为数字向量，生成traindata_vec.txt
import json
import sys, io
import jieba
import random
import re


sys.stdout = io.TextIOWrapper(sys.stdout.buffer,encoding='gb18030') #改变标准输出的默认编码

trainFile = 'law_data/my_traindata.json'#my_traindata my_testdata
stopwordFile = 'stopword.txt'
wordLabelFile = 'law_data/Output/wordLabel.txt'
trainDataVecFile = 'law_data/Output/traindata_vec.txt'#traindata_vec valdata_vec
maxLen = 50

labelFile = 'label.txt'


def read_labelFile(file):
    data = open(file, 'r', encoding='utf_8').read().split('\n')
    label_w2n = {}
    label_n2w = {}
    for line in data: #以 行 为单位
        #  加入下面两行代码，去掉BOM
        if line.startswith(u'\ufeff'):
            line = line.encode('utf8')[3:].decode('utf8')
        line = line.split(' ')  #指定分隔符对字符串进行切片，如果参数num有指定值，则分隔 num+1 个子字符串
        name_w = line[0]
        name_n = int(line[1])
        label_w2n[name_w] = name_n #保存类别对应的编号
        label_n2w[name_n] = name_w #保存类别名

    return label_w2n, label_n2w #先返回类别对应的编号，后返回类别名


def read_stopword(file):
    data = open(file, 'r', encoding='utf_8').read().split('\n')

    return data


def get_worddict(file):
    datas = open(file, 'r', encoding='utf_8').read().split('\n')
    datas = list(filter(None, datas)) #进行数据过滤空值None,默认会把0、false这样具体的值过滤掉。
    word2ind = {}
    for line in datas:
        line = line.split(' ')
        word2ind[line[0]] = int(line[1]) #列表，“某个词：序号”
    
    ind2word = {word2ind[w]:w for w in word2ind} #大表
    return word2ind, ind2word


def json2txt():
    label_dict, label_n2w = read_labelFile(labelFile)
    word2ind, ind2word = get_worddict(wordLabelFile)

    traindataTxt = open(trainDataVecFile, 'w')
    stoplist = read_stopword(stopwordFile)

    datas = open(trainFile, 'r', encoding='utf_8').readlines()
    datas = list(filter(None, datas))
    random.shuffle(datas)
    for line in datas:
        line = json.loads(line)
        # print(line)
        title = str(list(line.values())[0])
        cla = str(list(line.keys())[0])
        cla_ind = label_dict[cla]
        # print(cla, cla_ind,title)

        title_seg = jieba.cut(title, cut_all=False)
        title_ind = [cla_ind]

        for w in title_seg:
            w = re.sub(r"\s+", "", w)
            # w = w.replace(" ", "").replace("\n", "")
            if w not in stoplist:  # 剔除无用词
                for stopstr in stoplist:
                    stop_flag = 0
                    if stopstr in w:
                        stop_flag = 1
                        break
                if stop_flag == 0:
                    title_ind.append(word2ind[w])#第一个数字是类别，其余为切片后的句子向量。




        length = len(title_ind)
        if length > maxLen + 1:
            title_ind = title_ind[0:(maxLen+1)]
        if length < maxLen + 1:
            title_ind.extend([0] * (maxLen - length + 1))
        # print(title_ind)
        for n in title_ind:
            traindataTxt.write(str(n) + ',')
        traindataTxt.write('\n')
        if len(title_ind) != 51: print('Slice error!')


def main():
    json2txt()


if __name__ == "__main__":
    main()