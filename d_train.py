# -- coding: gbk --
import torch
import os
import torch.nn as nn
import numpy as np
import time

from d_model import textCNN  #从d_model包中只导入textCNN这个类
import c_sen2inds
import textCNN_data

word2ind, ind2word = c_sen2inds.get_worddict('law_data/Output/wordLabel.txt')
label_w2n, label_n2w = c_sen2inds.read_labelFile('label.txt') #w2n为编号，n2w为类别名

textCNN_param = {
    'vocab_size': len(word2ind), #词库大小，248402个单词
    'embed_dim': 60,  #每个索引对应一个嵌入维度为60的向量（相当于字典的key为1到？，每个key对应一个嵌入的维度为embed_dim=60的向量）
    'class_num': len(label_w2n), #最后一个全连接层所分出的类个数，10
    "kernel_num": 16,  #卷积核数量
    "kernel_size": [3, 4, 5], #卷积核尺寸
    "dropout": 0.5, #训练过程中，对于神经网络单元，按照0.5的概率将其暂时从网络中丢弃（防止过拟合）
}
dataLoader_param = {
    'batch_size': 128, #一次训练所选取的样本数（https://blog.csdn.net/qq_34886403/article/details/82558399）
    'shuffle': True,#打乱(SHUFFLE)。（https://www.freesion.com/article/6791948983/#2_shuffle_14）
}


def main():
    #init net
    print('init net...')
    net = textCNN(textCNN_param)
    weightFile = 'weight.pkl'
    if os.path.exists(weightFile):#判断括号里的文件是否存在，括号内的可以是文件路径
        print('load weight')
        net.load_state_dict(torch.load(weightFile, map_location=torch.device('cpu')))#CPU格式，所以括号内改成这样    torch.load(weightFile)
    else:
        net.init_weight()
    print(net)

   #net.cuda()

    #init dataset
    print('init dataset...')
    dataLoader = textCNN_data.textCNN_dataLoader(dataLoader_param)
    valdata = textCNN_data.get_valdata()

    optimizer = torch.optim.Adam(net.parameters(), lr=0.01)
    criterion = nn.NLLLoss()

    log = open('log_{}.txt'.format(time.strftime('%y%m%d%H')), 'w')
    log.write('epoch step loss\n')
    log_test = open('log_test_{}.txt'.format(time.strftime('%y%m%d%H')), 'w')
    log_test.write('epoch step test_acc\n')
    print("training...")
    for epoch in range(46):
        for i, (clas, sentences) in enumerate(dataLoader):
            optimizer.zero_grad()
            sentences = sentences.type(torch.LongTensor)
            clas = clas.type(torch.LongTensor)
            out = net(sentences)
            loss = criterion(out, clas)
            loss.backward()
            optimizer.step()

            if (i + 1) % 1 == 0:
                print("epoch:", epoch + 1, "step:", i + 1, "loss:", loss.item())
                data = str(epoch + 1) + ' ' + str(i + 1) + ' ' + str(loss.item()) + '\n'
                log.write(data)
        print("save model...")
        torch.save(net.state_dict(), weightFile)
        torch.save(net.state_dict(), "model\{}_model_iter_{}_{}_loss_{:.2f}.pkl".format(time.strftime('%y%m%d%H'), epoch, i, loss.item()))  # current is model.pkl
        print("epoch:", epoch + 1, "step:", i + 1, "loss:", loss.item())      


if __name__ == "__main__":
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    main()